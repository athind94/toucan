/*
 * gpio.h
 *
 *  Created on: Oct 13, 2016
 *      Author: athi
 */

#ifndef GPIO_H_
#define GPIO_H_

//#include <chip.h>
#include <toucan.h>
#include <canpie.h>
#include <error.h>

/* Public Defines */

typedef enum TC_GPIO_DIR
{
	TC_GPIO_DIR_INPUT = 0,
	TC_GPIO_DIR_OUTPUT
} TC_GPIO_DIR;

typedef enum TC_GPIO_FUNC
{
	TC_GPIO_FUNC_0 = 0,
	TC_GPIO_FUNC_1,
	TC_GPIO_FUNC_2
} TC_GPIO_FUNC;

typedef enum TC_GPIO_MODE
{
	TC_GPIO_MODE_NONE = 0,
	TC_GPIO_MODE_PULLDOWN,
	TC_GPIO_MODE_PULLUP,
	TC_GPIO_MODE_REPEATER
} TC_GPIO_MODE;

typedef enum TC_GPIO_INT_MODE
{
	TC_GPIO_INT_MODE_EDGE = 0,
	TC_GPIO_INT_MODE_LEVEL
} TC_GPIO_INT_MODE;

typedef enum TC_GPIO_INT_EDGE
{
	TC_GPIO_INT_EDGE_RISING = 0,
	TC_GPIO_INT_EDGE_FALLING,
	TC_GPIO_INT_EDGE_ALL
} TC_GPIO_INT_EDGE;

typedef enum TC_GPIO_INT_LEVEL
{
	TC_GPIO_INT_LEVEL_LOW = 0,
	TC_GPIO_INT_LEVEL_HIGH
} TC_GPIO_INT_LEVEL;



typedef void (*TC_GPIO_INTERRUPT_HANDLER)(); // Function pointer prototype

/* Public Functions */
tcErr tc_gpio_init(void);
tcErr tc_gpio_setPortDir(_U08 port, _U32 pinMask, TC_GPIO_DIR direction);
tcErr tc_gpio_setPinDir(_U08 port, _U08 pin, TC_GPIO_DIR direction);
tcErr tc_gpio_setPinFunction(_U08 port, _U08 pin, TC_GPIO_FUNC function);
tcErr tc_gpio_setPinMode(_U08 port, _U08 pin, TC_GPIO_MODE mode);
tcErr tc_gpio_setPinValue(_U08 port, _U08 pin, _U08 setting);
tcErr tc_gpio_togglePinValue(_U08 port, _U08 pin);

_U08 tc_gpio_getValue(_U08 port, _U08 pin);

tcErr tc_gpio_registerInterruptHandler(_U08 port, _U08 pin,
										TC_GPIO_INT_MODE mode,
										TC_GPIO_INT_EDGE edge,
										TC_GPIO_INT_LEVEL level,
										TC_GPIO_INTERRUPT_HANDLER handler);

tcErr tc_gpio_clearInterruptHandler(_U08 port, _U08 pin);

tcErr tc_gpio_enableInterrupt(_U08 port, _U08 pin);
tcErr tc_gpio_disableInterrupt(_U08 port, _U08 pin);



// ARCH SPECIFIC

// END ARCH SPECIFIC


#endif /* GPIO_H_ */
