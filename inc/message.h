/*
 * message.h
 *
 *  Created on: Oct 16, 2016
 *      Author: athi
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <toucan.h>

///* Priority Field Definitions */
//typedef enum TC_PRIORITY
//{
//	P_CRITICAL = 0,
//	P_CONTROL,
//	P_NETWORK,
//	P_TELEMETRY
//} TC_PRIORITY;
//
///* Message Type Definitions */
//typedef enum TC_MSG_TYPE
//{
//	MSG_TIMESYNC = 0,
//	MSG_COMMAND,
//	MSG_CONFIG,
//	MSG_BROADCAST,
//	MSG_ERROR,
//	MSG_WARNING,
//	MSG_HEARTBEAT,
//	MSG_NODE_MGMT
//} TC_MSG_TYPE;

/* Message Structure Definitions */
#define PRIO_BITS				2
#define PRIO_OFFSET				27
#define PRIO_BIT_MASK			((1 << PRIO_BITS) - 1)
#define GET_MSG_PRIORITY(id)	((id >> PRIO_OFFSET) & ((1 << PRI_BITS) - 1))

#define MSG_TYPE_BITS			7
#define MSG_TYPE_OFFSET			20
#define MSG_TYPE_MASK			((1 << MSG_TYPE_BITS) - 1)
#define GET_MSG_TYPE(id)		((id >> MSG_TYPE_OFFSET) & ((1 << MSG_TYPE_BITS) - 1))

#define NODE_FIELD_BITS			10
#define NODE_FIELD_OFFSET		10
#define NODE_FIELD_MASK			((1 << NODE_FIELD_BITS) - 1)
#define GET_NODE_FIELD(id)		((id >> NODE_FIELD_OFFSET) & ((1 << NODE_FIELD_BITS) - 1))

#define CHANNEL_FIELD_BITS		10
#define CHANNEL_FIELD_OFFSET	0
#define CHANNEL_FIELD_MASK		((1 << CHANNEL_FIELD_BITS) - 1)
#define GET_CHANNEL_FIELD(id)	((id >> CHANNEL_FIELD_OFFSET) & ((1 << CHANNEL_FIELD_BITS) - 1))

/* Timesync/Broadcast Message Structure */
#define SOURCE_ID_BITS		NODE_FIELD_BITS
#define SOURCE_ID_OFFSET	NODE_FIELD_OFFSET

#define CHANNEL_ID_BITS		CHANNEL_FIELD_BITS
#define CHANNEL_ID_OFFSET	CHANNEL_FIELD_OFFSET

/* Command Message Structure */
#define DEST_ID_BITS		NODE_FIELD_BITS
#define DEST_ID_OFFSET		NODE_FIELD_OFFSET

#define COMMAND_ID_BITS		CHANNEL_FIELD_BITS
#define COMMAND_ID_OFFSET	CHANNEL_FIELD_OFFSET

/* Config Message Structure */
//#define DEST_ID_BITS		NODE_FIELD_BITS
//#define DEST_ID_OFFSET	NODE_FIELD_OFFSET

#define CONFIG_ID_BITS		CHANNEL_FIELD_BITS
#define CONFIG_ID_OFFSET	CHANNEL_FIELD_OFFSET

/* Error Message Structure */
//#define SOURCE_ID_BITS	NODE_FIELD_BITS
//#define SOURCE_ID_OFFSET	NODE_FIELD_OFFSET

#define ERROR_ID_BITS		CHANNEL_FIELD_BITS
#define ERROR_ID_OFFSET		CHANNEL_FIELD_OFFSET

/* Warning Message Structure */
//#define SOURCE_ID_BITS	NODE_FIELD_BITS
//#define SOURCE_ID_OFFSET	NODE_FIELD_OFFSET

#define WARNING_ID_BITS		CHANNEL_FIELD_BITS
#define WARNING_ID_OFFSET	CHANNEL_FIELD_OFFSET

/* Heartbeat Message Structure */
//#define SOURCE_ID_BITS	NODE_FIELD_BITS
//#define SOURCE_ID_OFFSET	NODE_FIELD_OFFSET

/* Node Management Message Structure */
//#define DEST_ID_BITS		NODE_FIELD_BITS
//#define DEST_ID_OFFSET	NODE_FIELD_OFFSET

#define MGMT_ID_BITS		CHANNEL_FIELD_BITS
#define MGMT_ID_OFFSET		CHANNEL_FIELD_OFFSET

///* developer message_functions */
_U32 toucan_mk_timesync_id(void);
_U32 toucan_mk_command_id(_U16 dest_id, _U16 cmd_id);
_U32 toucan_mk_config_id(_U16 dest_id, _U16 config_id);
_U32 toucan_mk_broadcast_id(TC_PRIORITY prio, _U16 node_id, _U16 channel_id);
_U32 toucan_mk_error_id(_U16 node_id, _U16 error_id);
_U32 toucan_mk_warning_id(_U16 node_id, _U16 warning_id);
_U32 toucan_mk_heartbeat_id(void);
_U32 toucan_mk_node_mgmt_id(_U16 node_id, _U16 mgmt_id);

#endif /* MESSAGE_H_ */
