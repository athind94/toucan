/*
 * error.h
 *
 *  Created on: Oct 6, 2016
 *      Author: athi
 */

#ifndef ERROR_H_
#define ERROR_H_

typedef enum tcErr {
	tcErr_OK = 0,
	tcErr_HARDWARE,
	tcErr_NOT_SUPPORTED,
	tcErr_NOT_IMPLEMENTED,
	tcErr_INVALID_ARGUMENT,
	tcErr_INVALID_MODULE,
	tcErr_NO_FREE_HANDLERS,
	tcErr_INVALID_PIN,
	tcErr_NO_REGISTERED_HANDLER,
	tcErr_CAN_TX_BUFFER_FULL,
	tcErr_CAN_RX_BUFFER_FULL,
	tcErr_CAN_PERIODICS_FULL
} tcErr;


#endif /* ERROR_H_ */
