/*
 * toucan.h
 *
 *  Created on: Oct 6, 2016
 *      Author: athi
 */

#ifndef TOUCAN_H_
#define TOUCAN_H_

#include <compiler.h>
#include <chip.h>
#include <error.h>

typedef _U32 tc_time_t;

typedef enum tcMode
{
	TC_MODE_STOP = 0,
	TC_MODE_NORMAL,
	TC_MODE_MUTE,
	TC_MODE_DEAFEN
} tcMode;

/* Priority Field Definitions */
typedef enum TC_PRIORITY
{
	P_CRITICAL = 0,
	P_CONTROL,
	P_NETWORK,
	P_TELEMETRY
} TC_PRIORITY;

/* Message Type Definitions */
typedef enum TC_MSG_TYPE
{
	MSG_TIMESYNC = 0,
	MSG_COMMAND,
	MSG_CONFIG,
	MSG_BROADCAST,
	MSG_ERROR,
	MSG_WARNING,
	MSG_HEARTBEAT,
	MSG_NODE_MGMT
} TC_MSG_TYPE;

tcErr tc_init(_U16 node_id);
tcErr tc_set_mode(tcMode mode);
tcErr tc_handle(void);

_U16 toucan_get_node_id(void);

#endif /* TOUCAN_H_ */
