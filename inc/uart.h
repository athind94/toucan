/*
 * uart.h
 *
 *  Created on: Oct 12, 2016
 *      Author: athi
 */

#ifndef UART_H_
#define UART_H_

#include <chip.h>
#include <toucan.h>
#include <canpie.h>

#include <stdarg.h>

// These are hardware specific and will be moved to arch specific header when created

typedef LPC_USART_T	TC_UART_T;
#define TC_NUM_UART_MODULES	3

#define TC_UART0	LPC_USART0
#define TC_UART1	LPC_USART1
#define TC_UART2	LPC_USART2

#define UART_CFG_DATALEN_7      (0x00 << 2)	/*!< UART 7 bit length mode */
#define UART_CFG_DATALEN_8      (0x01 << 2)	/*!< UART 8 bit length mode */
#define UART_CFG_DATALEN_9      (0x02 << 2)	/*!< UART 9 bit length mode */
#define UART_CFG_PARITY_NONE    (0x00 << 4)	/*!< No parity */
#define UART_CFG_PARITY_EVEN    (0x02 << 4)	/*!< Even parity */
#define UART_CFG_PARITY_ODD     (0x03 << 4)	/*!< Odd parity */
#define UART_CFG_STOPLEN_1      (0x00 << 6)	/*!< UART One Stop Bit Select */
#define UART_CFG_STOPLEN_2      (0x01 << 6)	/*!< UART Two Stop Bits Select */

#define UART_USB_TX_PORT	0
#define UART_USB_TX_PIN		18
#define UART_USB_RX_PORT	0
#define UART_USB_RX_PIN		13

// end arch specific declarations

typedef enum TC_UART_CFG_DATALEN
{
	TC_UART_CFG_DATALEN_7 = 0,
	TC_UART_CFG_DATALEN_8,
	TC_UART_CFG_DATALEN_9
} TC_UART_CFG_DATALEN;

typedef enum TC_UART_CFG_PARITY
{
	TC_UART_CFG_PARITY_NONE = 0,
	TC_UART_CFG_PARITY_EVEN,
	TC_UART_CFG_PARITY_ODD
} TC_UART_CFG_PARITY;

typedef enum TC_UART_CFG_STOPLEN
{
	TC_UART_CFG_STOPLEN_1 = 0,
	TC_UART_CFG_STOPLEN_2
} TC_UART_CFG_STOPLEN;

// Public functions

tcErr tc_uart_debug_assign(TC_UART_T* module);
tcErr tc_uart_tx_pinassign(TC_UART_T* module, _U08 port, _U08 pin);
tcErr tc_uart_rx_pinassign(TC_UART_T* module, _U08 port, _U08 pin);

tcErr tc_uart_init(TC_UART_T* module, _U32 baudrate,
					TC_UART_CFG_DATALEN datalen,
					TC_UART_CFG_PARITY parity,
					TC_UART_CFG_STOPLEN stopbits);

tcErr tc_uart_stop(TC_UART_T* module);

_S32 tc_uart_send(TC_UART_T* module, _U08 *data, _S32 numBytes);
_S32 tc_uart_sendBlocking(TC_UART_T* module, _U08 *data, _S32 numBytes);
_S32 tc_uart_read(TC_UART_T* module, _U08 *data, _S32 numBytes);
_S32 tc_uart_readBlocking(TC_UART_T* module, _U08 *data, _S32 numBytes);
_S32 tc_uart_printf(const char *format, ...);

#endif /* UART_H_ */
