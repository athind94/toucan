/*
 * can.h
 *
 *  Created on: Oct 16, 2016
 *      Author: athi
 */

#ifndef TC_CAN_H_
#define TC_CAN_H_

//#include <canpie.h>
#include <error.h>
#include <toucan.h>

/* Public variables and types */
typedef void (*TC_INBOX_HANDLER)(_S32 data, tc_time_t timestamp);
typedef void (*TC_COMMAND_HANDLER)(_S32 data, tc_time_t timestamp);
typedef void (*TC_GLOBAL_HANDLER)(_U32 id, _U64 data);

/* Public Functions */
tcErr tc_send_broadcast(TC_PRIORITY priority, _U16 chan_num, _S32 data);
tcErr tc_send_warning(_U16 warning_id, _S32 data);
tcErr tc_send_error(_U16 error_id, _S32 data);
tcErr tc_send_command(_U16 dest_node, _U16 cmd_id, _S32 data);
tcErr tc_send_broadcast_periodic(TC_PRIORITY priority, _U16 chan_num, _S32 * data_ptr, tc_time_t period);
tcErr tc_assign_inbox_handler(TC_MSG_TYPE msg_type,
								_U16 node_id,
								_U16 chan_num,
								TC_INBOX_HANDLER handler);
tcErr tc_assign_command_handler(_U16 cmd_id, TC_COMMAND_HANDLER handler);

tcErr tc_send_generic_ext(_U32 id, _U08 *data, _U08 DLC);
tcErr tc_send_generic_std(_U16 id, _U08 *data, _U08 DLC);
tcErr tc_assign_global_handler(TC_GLOBAL_HANDLER handler);

/* Developer Functions */
tcErr tc_can_init(_U08 baudRate);
tcErr toucan_send_heartbeat(void);
tcErr toucan_handle_periodics(void);
tcErr toucan_send_queued_messages(void);


#endif /* CAN_H_ */
