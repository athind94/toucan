/*
 * timer.h
 *
 *  Created on: Oct 6, 2016
 *      Author: athi
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <toucan.h>
#include <error.h>

//typedef _U32 tc_time_t;

tcErr 		tc_timer_init(void);
tc_time_t 	tc_timer_get(void);
tcErr 		tc_timer_set_timesync(tc_time_t time);
tc_time_t 	tc_timer_get_timesync(void);
tcErr		tc_timer_delay_ms(_U32 delay);
tcErr 		tc_timer_blocking_delay_ms(_U32 delay);


#endif /* TIMER_H_ */
