# Toucan

Toucan is a hardware abstraction library for microcontrollers with an emphasis on CAN communications, supplying an easy-to-use yet powerful telemetry driven CAN protocol as well as interfaces to most commonly found external microcontroller hardware modules. It has been built to support the UNSW Sunswift Solar Racing Team though can be adapted for other uses.


## CAN Protocol Description

Messages in the Toucan protocol can be split into two primary forms of communication, *Broadcast* and *Targeted*. *Broadcast* messaging is when a source node takes a measurement and sends it out to all nodes on the network. In this case node programmers can designate what broadcast messages each nodes is interested in, and can write a callback function to be executed upon receival of a broadcast message from a particular measurement source. 

> Example: MOTOR CONTROLLER node and ACCELERATOR PEDAL node are separate, yet the MOTOR CONTROLLER is clearly interested in the current measurement of the ACCELERATOR PEDAL. ACCELERATOR PEDAL periodically sends out broadcast messages of the current depression value of the pedal. MOTOR CONTROLLER node sees this is as an interesting measurement, and calls a user-written function which adjusts torque based on the depression value upon receival of each message.

Targeted messages are point-to-point, that is they are directed to only a single node. These messages are used when a node wants to trigger an event which is guaranteed to only be pertinent to a single node. Node designers can designate callback functions upon receival of a specific command.

> Example: DASHBOARD node has function to execute a global IV sweep for the Maximum Power Point Tracker (MPPT) node. Sends a command message, the MPPT detects the command and executes any defined callbacks.

The observant reader will notice that from a network perspective, identical functionality can be trivially achieved using either *Broadcast* or *Targeted* messages. The reason they are separate is two-fold:

- *Targeted* messages are infrequent in a realistic network, whereas *Broadcast* messages are expected to be frequent (>5 times a second). The infrequency of *targeted* messages means dedicating a hardware buffer in a CAN controller would be wasteful while not improving performance by any significant degree.
- *Targeted* messages are not intended to be visualised in the same way as a Broadcast message (which are almost excluxively telemetry measurements). Thus a clear separation allows for a simpler, and more elegant data visualisation design for strategy purposes.

Specific details of message types, packet formats and prescribed node behaviour can be found in the thesis and API attached within the documents folder.

## Supported Microprocessors

- LPC15XX
- ...*more to come*

## Installation
Currently LPCXpresso is required. Tom Dransfield is working to port it away from a specific IDE, at that point installation instructions will be updated.

## FAQs
*..will be periodically updated once people start using Toucan.*

## TODO
Tasks in descending order of priority.

- Implement CONFIG messages (need an EEPROM library implementation) \- *Unassigned*
- Define a basic test network configuartion and setup a series of unit tests \- *Unassigned*
- SPI hardware module \- *Unassigned*
- ADC hardware module \- *Unassigned*
- Migrate Toucan away from LPCXpresso \- *Tom Dransfield (tom.dransfield@sunswift.unsw.edu.au)*

## Authors

#### Original Author
- Anmol Thind (a.thind94@gmail.com)

#### Development Lead
- Tom Dransfield (tom.dransfield@sunswift.unsw.edu.au)

#### Contributors
- *...could be you!*






