/*
 * can.c
 *
 *  Created on: Oct 16, 2016
 *      Author: athi
 */

#include <cp_core.h>
#include <cp_msg.h>

#include <message.h>

#include <error.h>
#include <timer.h>
#include <uart.h>
#include <can.h>

#include <toucan.h>

/* COMMENT THIS OUT FOR PRODUCTION CODE */
#define CAN_DEBUG

#define TC_CAN_TX_BUFFER_SIZE 		32
#define TC_CAN_PERIODIC_LIMIT		32
#define TC_CAN_RX_MAX_BUFFERS		20
#define TC_COMMAND_HANDLER_LIMIT	32

#define TC_TIMESYNC_INBOX_ID		0
#define TC_NODE_MGMT_INBOX_ID		1
#define TC_CONFIG_INBOX_ID			2
#define TC_COMMAND_INBOX_ID			3

#define TC_NUM_DEFAULT_HANDLERS		4

_U08 count;


typedef struct tc_periodic_broadcast
{
	_TsCpCanMsg 	msg;
	_S32 			*data_ptr;
	tc_time_t 		interval;
	tc_time_t		last_sent_time;
} tc_periodic_broadcast;


_U08 globalRcvHandler(_TsCpCanMsg * msg, _U08 buffNum);
void globalCmdHandler(_U16 cmd_id, _S32 data, tc_time_t timestamp);

tcErr toucan_queue_tx_message(_TsCpCanMsg *msg);


/* Global Variables */
_TsCpPort 				tcPort;

_TsCpCanMsg 			*transmit_queue[TC_CAN_TX_BUFFER_SIZE];
tc_periodic_broadcast 	pd_broadcast_queue[TC_CAN_PERIODIC_LIMIT];

TC_INBOX_HANDLER 		inbox_handlers[TC_CAN_RX_MAX_BUFFERS];
TC_COMMAND_HANDLER		command_handlers[TC_COMMAND_HANDLER_LIMIT];

_U08 queue_front;
_U08 queue_size;
_U08 pd_bc_count = 0;
_U08 rx_buffer_next = TC_NUM_DEFAULT_HANDLERS;

_U08 debug_errors;

/* Interrupt Functions */
_U08 globalRcvHandler(_TsCpCanMsg *msg, _U08 buffNum)
{
#ifdef CAN_DEBUG
	tc_uart_printf("RCV -> ID: %u, DATA: %u, TS: %u", msg->tuMsgId.ulExt, msg->tuMsgData.aulLong[0], msg->tuMsgData.aulLong[1]);
#endif
	// Find if it's a default or user defined inbox
	if (buffNum >= TC_NUM_DEFAULT_HANDLERS) {
		// User defined inbox
#ifdef CAN_DEBUG
		tc_uart_printf(" (USER DEF. INBOX)\n");
#endif
		_S32 data = msg->tuMsgData.aulLong[0];
		tc_time_t timestamp = msg->tuMsgData.aulLong[1];
		inbox_handlers[buffNum](data, timestamp);
	}
	else if (buffNum == TC_TIMESYNC_INBOX_ID){
		// Timesync inbox
#ifdef CAN_DEBUG
		tc_uart_printf(" (TIMESYNC)\n");
#endif
		tc_timer_set_timesync(msg->tuMsgData.aulLong[0]);
	}
	else if (buffNum == TC_CONFIG_INBOX_ID) {
		// Configuration inbox
#ifdef CAN_DEBUG
		tc_uart_printf(", (CONFIG)\n");
		tc_uart_printf("TODO: Config still not implemented\n");
#endif
	}
	else if (buffNum == TC_COMMAND_INBOX_ID) {
		// Command Inbox
#ifdef CAN_DEBUG
		tc_uart_printf(" (COMMAND)\n");
#endif
		_S32 data = msg->tuMsgData.aulLong[0];
		tc_time_t timestamp = msg->tuMsgData.aulLong[1];
		globalCmdHandler(GET_CHANNEL_FIELD(msg->tuMsgId.ulExt), data, timestamp);
	}
	else {
#ifdef CAN_DEBUG
		tc_uart_printf("Something terribly wrong has happened in the receive handler\n");
#endif
	}

	return tcErr_OK;
}

void globalCmdHandler(_U16 cmd_id, _S32 data, tc_time_t timestamp)
{
	if (cmd_id >= TC_COMMAND_HANDLER_LIMIT) {
#ifdef CAN_DEBUG
		tc_uart_printf("ERR -> Invalid Command Identifier\n");
#endif
		return;
	}

	if (command_handlers[cmd_id] != 0) {
		command_handlers[cmd_id](data, timestamp);
	}
	else {
#ifdef CAN_DEBUG
		tc_uart_printf("ERR -> Unassigned Command Identifier\n");
#endif
	}
}

/*
 * tc_can_init
 *
 * Call this function to initialise CAN operation.
 * baudRate should be taken from the CP_BAUD enum in canpie.h
 */

tcErr tc_can_init(_U08 baudRate)
{
    /* Start CAN Operation */
    tcPort = 1;

    if (CpCoreDriverInit(1, &tcPort) == CpErr_HARDWARE) {
    	__WFI();
    }

    // Set Baudrate
    CpCoreBaudrate(&tcPort, baudRate);

    // Start normal operation
    CpCoreCanMode(&tcPort, CP_MODE_START);

    // Install interrupt functions
    CpCoreIntFunctions(&tcPort, globalRcvHandler, NULL, NULL);

    /* Install default inboxes */

    // TIMESYNC INBOX
    _TsCpCanMsg msg;
    CpMsgClear(&msg);

    CpMsgSetExtId(&msg, toucan_mk_timesync_id());
    CpMsgSetDlc(&msg, 8);
	CpMsgSetData(&msg, 0, 0x00);
	CpMsgSetData(&msg, 1, 0x00);
	CpMsgSetData(&msg, 2, 0xF0);
	CpMsgSetData(&msg, 3, 0x1F);

	CpCoreBufferInit(&tcPort, &msg, TC_TIMESYNC_INBOX_ID, CP_BUFFER_DIR_RX);

	// NODE MGMT INBOX
    CpMsgClear(&msg);

    CpMsgSetExtId(&msg, toucan_mk_node_mgmt_id(toucan_get_node_id(), 0));
    CpMsgSetDlc(&msg, 8);
	CpMsgSetData(&msg, 0, 0x00);
	CpMsgSetData(&msg, 1, 0xFC);
	CpMsgSetData(&msg, 2, 0xFF);
	CpMsgSetData(&msg, 3, 0x1F);

	CpCoreBufferInit(&tcPort, &msg, TC_NODE_MGMT_INBOX_ID, CP_BUFFER_DIR_RX);

	// CONFIG INBOX
    CpMsgClear(&msg);

    CpMsgSetExtId(&msg, toucan_mk_config_id(toucan_get_node_id(), 0));
    CpMsgSetDlc(&msg, 8);
	CpMsgSetData(&msg, 0, 0x00);
	CpMsgSetData(&msg, 1, 0xFC);
	CpMsgSetData(&msg, 2, 0xFF);
	CpMsgSetData(&msg, 3, 0x1F);

	CpCoreBufferInit(&tcPort, &msg, TC_CONFIG_INBOX_ID, CP_BUFFER_DIR_RX);

	// COMMAND INBOX
    CpMsgClear(&msg);

    CpMsgSetExtId(&msg, toucan_mk_command_id(toucan_get_node_id(), 0));
    CpMsgSetDlc(&msg, 8);
	CpMsgSetData(&msg, 0, 0x00);
	CpMsgSetData(&msg, 1, 0xFC);
	CpMsgSetData(&msg, 2, 0xFF);
	CpMsgSetData(&msg, 3, 0x1F);

	CpCoreBufferInit(&tcPort, &msg, TC_COMMAND_INBOX_ID, CP_BUFFER_DIR_RX);

    return tcErr_OK;
}

tcErr tc_send_broadcast(TC_PRIORITY priority, _U16 chan_num, _S32 data)
{
	// Make a CpMsg
	_TsCpCanMsg my_msg;
	CpMsgClear(&my_msg);

	// Set the ID and Data Length Code (DLC) in the CP msg
	CpMsgSetExtId(&my_msg, toucan_mk_broadcast_id(priority, toucan_get_node_id(), chan_num));
	CpMsgSetDlc(&my_msg, 8);

	my_msg.tuMsgData.aulLong[0] = data;
	my_msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	count++;

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &my_msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&my_msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (BROADCAST)", my_msg.tuMsgId.ulExt, my_msg.tuMsgData.aulLong[0], my_msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_send_warning(_U16 warning_id, _S32 data)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	// Set the ID and Data Length COde (DLC) in the CP msg
	CpMsgSetExtId(&msg, toucan_mk_warning_id(toucan_get_node_id(), warning_id));
	CpMsgSetDlc(&msg, 8);

	msg.tuMsgData.aulLong[0] = data;
	msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (WARNING)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_send_error(_U16 error_id, _S32 data)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	// Set the ID and Data Length COde (DLC) in the CP msg
	CpMsgSetExtId(&msg, toucan_mk_error_id(toucan_get_node_id(), error_id));
	CpMsgSetDlc(&msg, 8);

	msg.tuMsgData.aulLong[0] = data;
	msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (ERROR)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_send_command(_U16 dest_node, _U16 cmd_id, _S32 data)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	// Set the ID and Data Length Code (DLC) in the CP msg
	CpMsgSetExtId(&msg, toucan_mk_command_id(dest_node, cmd_id));
	CpMsgSetDlc(&msg, 8);

	msg.tuMsgData.aulLong[0] = data;
	msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (COMMAND)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_send_broadcast_periodic(TC_PRIORITY priority, _U16 chan_num, _S32 * data_ptr, tc_time_t period)
{
	// Check if our periodics are full
	if (pd_bc_count >= TC_CAN_PERIODIC_LIMIT) {
		return tcErr_CAN_PERIODICS_FULL;
	}

	tc_periodic_broadcast new_bc;

	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	// Set the ID and Data Length Code (DLC) in the CP msg
	CpMsgSetExtId(&msg, toucan_mk_broadcast_id(priority, toucan_get_node_id(), chan_num));
	CpMsgSetDlc(&msg, 8);

	msg.tuMsgData.aulLong[0] = *data_ptr;
	msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	// Send msg for first time
	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (BROADCAST PERIODIC)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	// Register it as a new periodic broadcast
	new_bc.msg = msg;
	new_bc.interval = period;
	new_bc.last_sent_time = tc_timer_get_timesync();
	new_bc.data_ptr = data_ptr;

	pd_broadcast_queue[pd_bc_count] = new_bc;
	pd_bc_count++;

	return tcErr_OK;
}

tcErr tc_assign_inbox_handler(TC_MSG_TYPE msg_type,
								_U16 node_id,
								_U16 chan_num,
								TC_INBOX_HANDLER handler)
{
	if (rx_buffer_next >= TC_CAN_RX_MAX_BUFFERS) {
		return tcErr_CAN_RX_BUFFER_FULL;
	}

	// Make a matching CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	switch (msg_type) {
	case MSG_BROADCAST:
		CpMsgSetExtId(&msg, toucan_mk_broadcast_id(P_CONTROL, node_id, chan_num));
		CpMsgSetDlc(&msg, 8);

		// Hacky: have to load filter mask into lower byte of data
		// Want to use every bit to arbitrate except priority field
		CpMsgSetData(&msg, 0, 0xFF);
		CpMsgSetData(&msg, 1, 0xFF);
		CpMsgSetData(&msg, 2, 0xFF);
		CpMsgSetData(&msg, 3, 0x07);
		break;

	case MSG_ERROR:
		CpMsgSetExtId(&msg, toucan_mk_error_id(node_id, chan_num));
		CpMsgSetDlc(&msg, 8);

		// Hacky: have to load filter mask into lower byte of data
		// Want to use every bit to arbitrate
		CpMsgSetData(&msg, 0, 0xFF);
		CpMsgSetData(&msg, 1, 0xFF);
		CpMsgSetData(&msg, 2, 0xFF);
		CpMsgSetData(&msg, 3, 0x1F);
		break;

	case MSG_WARNING:
		CpMsgSetExtId(&msg, toucan_mk_warning_id(node_id, chan_num));
		CpMsgSetDlc(&msg, 8);

		// Hacky: have to load filter mask into lower byte of data
		// Want to use every bit to arbitrate
		CpMsgSetData(&msg, 0, 0xFF);
		CpMsgSetData(&msg, 1, 0xFF);
		CpMsgSetData(&msg, 2, 0xFF);
		CpMsgSetData(&msg, 3, 0x1F);
		break;

	default:
		return tcErr_INVALID_ARGUMENT;
		break;
	}

	// Assign handler
	inbox_handlers[rx_buffer_next] = handler;

	CpCoreBufferInit(&tcPort, &msg, rx_buffer_next, CP_BUFFER_DIR_RX);
	rx_buffer_next++;

	return tcErr_OK;
}

tcErr tc_assign_command_handler(_U16 cmd_id, TC_COMMAND_HANDLER handler)
{
	if (cmd_id >= TC_COMMAND_HANDLER_LIMIT) {
		return tcErr_INVALID_ARGUMENT;
	}

	command_handlers[cmd_id] = handler;
	return tcErr_OK;
}

tcErr tc_send_generic_ext(_U32 id, _U08 *data, _U08 DLC)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	CpMsgSetExtId(&msg, id);
	CpMsgSetDlc(&msg, DLC);

	for (int i = 0; i < DLC; i++) {
		CpMsgSetData(&msg, i, *data);
		data++;
	}

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (GENERIC EXT. FRAME)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_send_generic_std(_U16 id, _U08 *data, _U08 DLC)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	CpMsgSetStdId(&msg, id);
	CpMsgSetDlc(&msg, DLC);

	for (int i = 0; i < DLC; i++) {
		CpMsgSetData(&msg, i, *data);
		data++;
	}

	_U32 bufsize = 1;

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (GENERIC STD. FRAME)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr tc_assign_global_handler(TC_GLOBAL_HANDLER handler)
{
	return tcErr_NOT_IMPLEMENTED;
}

tcErr toucan_send_heartbeat(void)
{
	// Make a CpMsg
	_TsCpCanMsg msg;
	CpMsgClear(&msg);

	// Set the ID and Data Length Code (DLC) in the CP msg
	CpMsgSetExtId(&msg, toucan_mk_heartbeat_id());
	CpMsgSetDlc(&msg, 8);

	msg.tuMsgData.aulLong[0] = 0;
	msg.tuMsgData.aulLong[1] = tc_timer_get_timesync();

	_U32 bufsize = 1;

	tc_uart_printf("%u,%u,%u,pkt", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);

	// Try send message straight away, if buffer is full queue it
	if(CpCoreMsgWrite(&tcPort, &msg, &bufsize) == CpErr_TRM_FULL){
		toucan_queue_tx_message(&msg);
	}

#ifdef CAN_DEBUG
	tc_uart_printf("TX -> ID: %u, DATA: %u, TS: %u (HEARTBEAT)", msg.tuMsgId.ulExt, msg.tuMsgData.aulLong[0], msg.tuMsgData.aulLong[1]);
#endif

	return tcErr_OK;
}

tcErr toucan_queue_tx_message(_TsCpCanMsg *msg)
{
	if (queue_size == TC_CAN_TX_BUFFER_SIZE) {
#ifdef CAN_DEBUG
		tc_uart_printf("TX -> packet dropped (BUFFER FULL)\n");
#endif
		return tcErr_CAN_TX_BUFFER_FULL;
	}

	_U08 queue_pos = (queue_front + queue_size) % TC_CAN_TX_BUFFER_SIZE;
	transmit_queue[queue_pos] = msg;

	return tcErr_OK;
}

/*
 * toucan_send_queue_messages
 *
 * Floods the CAN controller with as many queued messages as possible
 * Stop when CAN controller message buffers are full or queue is empty
 */
tcErr toucan_send_queued_messages(void)
{
	if (queue_size == 0) {
		return tcErr_OK;
	}

	_TsCpCanMsg *currMsg = transmit_queue[queue_front];
	_U32 bufsize = 1;

	while (CpCoreMsgWrite(&tcPort, currMsg, &bufsize) != CpErr_TRM_FULL) {
		queue_front = (queue_front + 1) % TC_CAN_TX_BUFFER_SIZE; // Move queue up
		queue_size--; // Decrement queue size

		// Check if queue is empty
		if (queue_size == 0) {
			break;
		}

		currMsg = transmit_queue[queue_front];
	}

	return tcErr_OK;
}

tcErr toucan_handle_periodics(void)
{
	_U08 i;
	tc_periodic_broadcast *currBC;

	for (i = 0; i < pd_bc_count; i++) {
		currBC = &pd_broadcast_queue[i];
		if (tc_timer_get() - currBC->last_sent_time > currBC->interval) {
			currBC->msg.tuMsgData.aulLong[0] = *(currBC->data_ptr);
			currBC->msg.tuMsgData.aulLong[1] = tc_timer_get();

			_U32 bufsize = 1;

			// Try send message straight away, if buffer is full queue it
			if(CpCoreMsgWrite(&tcPort, &(currBC->msg), &bufsize) == CpErr_TRM_FULL){
				toucan_queue_tx_message(&(currBC->msg));
			}
			currBC->last_sent_time = tc_timer_get();
		}
	}

	return tcErr_OK;
}
