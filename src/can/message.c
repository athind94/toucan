/*
 * message.c
 *
 *  Created on: Oct 16, 2016
 *      Author: athi
 */

#include <toucan.h>
#include <message.h>

/*
 * Quick Private functions for dealing with Toucan and CANpie message conversion
 */

_U32 toucan_mk_timesync_id(void)
{
	return  (((_U32)(P_CRITICAL & PRIO_BIT_MASK) << PRIO_OFFSET) |
			 ((_U32)(MSG_TIMESYNC & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			 ((_U32)(toucan_get_node_id() & NODE_FIELD_MASK) << NODE_FIELD_OFFSET) |
			 ((_U32)(0 & CHANNEL_FIELD_MASK) << CHANNEL_FIELD_OFFSET));
}

_U32 toucan_mk_command_id(_U16 dest_id, _U16 cmd_id)
{
	return 	(((_U32)P_CONTROL & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_COMMAND & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)dest_id & NODE_FIELD_MASK) << NODE_FIELD_OFFSET) |
			(((_U32)cmd_id & CHANNEL_FIELD_MASK) << CHANNEL_FIELD_OFFSET);
}

_U32 toucan_mk_config_id(_U16 dest_id, _U16 config_id)
{
	return 	(((_U32)P_CONTROL & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_CONFIG & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)dest_id & NODE_FIELD_MASK) << NODE_FIELD_OFFSET) |
			(((_U32)config_id & CHANNEL_FIELD_MASK) << CHANNEL_FIELD_OFFSET);
}

_U32 toucan_mk_broadcast_id(TC_PRIORITY prio, _U16 node_id, _U16 channel_id)
{
	return 	(((_U32)prio & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_BROADCAST & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)node_id & NODE_FIELD_MASK) << SOURCE_ID_OFFSET) |
			(((_U32)channel_id & CHANNEL_FIELD_MASK) << CHANNEL_ID_OFFSET);
}

_U32 toucan_mk_error_id(_U16 node_id, _U16 error_id)
{
	return 	(((_U32)P_NETWORK & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_ERROR & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)node_id & NODE_FIELD_MASK) << SOURCE_ID_OFFSET) |
			(((_U32)error_id & CHANNEL_FIELD_MASK) << CHANNEL_ID_OFFSET);
}

_U32 toucan_mk_warning_id(_U16 node_id, _U16 warning_id)
{
	return 	(((_U32)P_NETWORK & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_WARNING & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)node_id & NODE_FIELD_MASK) << SOURCE_ID_OFFSET) |
			(((_U32)warning_id & CHANNEL_FIELD_MASK) << CHANNEL_ID_OFFSET);
}

_U32 toucan_mk_heartbeat_id(void)
{
	return 	(((_U32)P_NETWORK & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_HEARTBEAT & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)toucan_get_node_id() & NODE_FIELD_MASK) << SOURCE_ID_OFFSET) |
			(((_U32)0 & CHANNEL_FIELD_MASK) << CHANNEL_ID_OFFSET);
}

_U32 toucan_mk_node_mgmt_id(_U16 node_id, _U16 mgmt_id)
{
	return 	(((_U32)P_NETWORK & PRIO_BIT_MASK) << PRIO_OFFSET) |
			(((_U32)MSG_NODE_MGMT & MSG_TYPE_MASK) << MSG_TYPE_OFFSET) |
			(((_U32)node_id & NODE_FIELD_MASK) << SOURCE_ID_OFFSET) |
			(((_U32)mgmt_id & CHANNEL_FIELD_MASK) << CHANNEL_ID_OFFSET);
}
