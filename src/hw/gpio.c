/*
 * gpio.c
 *
 *  Created on: Oct 13, 2016
 *      Author: athi
 */

//#include <toucan.h>
#include <gpio.h>
#include <error.h>

#define MAX_HANDLERS 8

TC_GPIO_INTERRUPT_HANDLER handlers[MAX_HANDLERS];
_U08 handler_pins[MAX_HANDLERS];

tcErr tc_gpio_init(void)
{
	/* Enable clocks to all GPIO ports and input mux */
	Chip_GPIO_Init(LPC_GPIO);

	/* Enable pin interrupt register */
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	return tcErr_OK;
}

tcErr tc_gpio_setPortDir(_U08 port, _U32 pinMask, TC_GPIO_DIR direction)
{
	Chip_GPIO_SetPortDIR(LPC_GPIO, port, pinMask, direction);
	return tcErr_OK;
}

tcErr tc_gpio_setPinDir(_U08 port, _U08 pin, TC_GPIO_DIR direction)
{
	Chip_GPIO_SetPinDIR(LPC_GPIO, port, pin, direction);
	return tcErr_OK;
}

tcErr tc_gpio_setPinFunction(_U08 port, _U08 pin, TC_GPIO_FUNC function)
{
	// LPC15xx Chip doesn't have multi-function pins
	return tcErr_NOT_SUPPORTED;
}

tcErr tc_gpio_setPinMode(_U08 port, _U08 pin, TC_GPIO_MODE mode)
{
	Chip_IOCON_PinMuxSet(LPC_IOCON, port, pin,
						(IOCON_DIGMODE_EN | (mode << 3)));
	return tcErr_OK;
}


tcErr tc_gpio_setPinValue(_U08 port, _U08 pin, _U08 setting)
{
	Chip_GPIO_SetPinState(LPC_GPIO, port, pin, setting);
	return tcErr_OK;
}

tcErr tc_gpio_togglePinValue(_U08 port, _U08 pin)
{
	Chip_GPIO_SetPinToggle(LPC_GPIO, port, pin);
	return tcErr_OK;
}

_U08 tc_gpio_getValue(_U08 port, _U08 pin)
{
	return Chip_GPIO_GetPinState(LPC_GPIO, port, pin);
}

tcErr tc_gpio_registerInterruptHandler(_U08 port, _U08 pin,
										TC_GPIO_INT_MODE mode,
										TC_GPIO_INT_EDGE edge,
										TC_GPIO_INT_LEVEL level,
										TC_GPIO_INTERRUPT_HANDLER handler)
{
	_S32 i;

	/* Scan for a free handler slot */
	for (i = 0; i < MAX_HANDLERS; i++) {
		if (handlers[i] == 0) {
			break;
		}
	}

	if (i == MAX_HANDLERS) {
		return tcErr_NO_FREE_HANDLERS;
	}

	/* Only ports 0 and 1 can have interrupts */
	if (((port * 32) + pin) > 63) {
		return tcErr_INVALID_PIN;
	}

	/* Remember what port, pin pair correspond to what handler */
	handler_pins[i] = (port*32) + pin;
	handlers[i] = handler;

	/* Set pin to GPIO */
	Chip_IOCON_PinMuxSet(LPC_IOCON, port, pin,
						(IOCON_DIGMODE_EN | IOCON_MODE_INACT));

	/* Configure GPIO pin as input */
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, port, pin);

	/* Set pin to corresponding pin interrupt vector */
	Chip_INMUX_PinIntSel(i, port, pin);

	/* Clear interrupt status just in case */
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(i));

	/* Set interrupt detection type */
	if (mode == TC_GPIO_INT_MODE_EDGE) {
		Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH(i));
		if (edge == TC_GPIO_INT_EDGE_RISING) {
			Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH(i));
		}
		else if (edge == TC_GPIO_INT_EDGE_FALLING) {
			Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH(i));
		}
		else if (edge == TC_GPIO_INT_EDGE_ALL){
			Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH(i));
			Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH(i));
		}
		else {
			return tcErr_INVALID_ARGUMENT;
		}
	}
	else if (mode == TC_GPIO_INT_MODE_LEVEL) {
		Chip_PININT_SetPinModeLevel(LPC_GPIO_PIN_INT, PININTCH(i));
		if (level == TC_GPIO_INT_LEVEL_LOW) {
			Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH(i));
		}
		else if (level == TC_GPIO_INT_LEVEL_HIGH) {
			Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH(i));
		}
		else {
			return tcErr_INVALID_ARGUMENT;
		}
	}
	else {
		return tcErr_INVALID_ARGUMENT;
	}

	return tcErr_OK;
}

tcErr tc_gpio_clearInterruptHandler(_U08 port, _U08 pin)
{
	_S32 i;
	_U08 pinid = (port * 32) + pin;

	for (i = 0; i < MAX_HANDLERS; i++) {
		if (handler_pins[i] == pinid) {
			NVIC_ClearPendingIRQ(PIN_INT0_IRQn + i);
			NVIC_DisableIRQ(PIN_INT0_IRQn + i);
			handlers[i] = 0;
			handler_pins[i] = 0;
			return tcErr_OK;
		}
	}
	return tcErr_NO_REGISTERED_HANDLER;
}

tcErr tc_gpio_enableInterrupt(_U08 port, _U08 pin)
{
	_S32 i;
	_U08 pinid = (port * 32) + pin;

	for (i = 0; i < MAX_HANDLERS; i++) {
		if (handler_pins[i] == pinid) {
			NVIC_ClearPendingIRQ(PIN_INT0_IRQn + i);
			NVIC_EnableIRQ(PIN_INT0_IRQn + i);
			return tcErr_OK;
		}
	}
	return tcErr_NO_REGISTERED_HANDLER;
}

tcErr tc_gpio_disableInterrupt(_U08 port, _U08 pin)
{
	_S32 i;
	_U08 pinid = (port * 32) + pin;

	for (i = 0; i < MAX_HANDLERS; i++) {
		if (handler_pins[i] == pinid) {
			NVIC_ClearPendingIRQ(PIN_INT0_IRQn + i);
			NVIC_DisableIRQ(PIN_INT0_IRQn + i);
			return tcErr_OK;
		}
	}
	return tcErr_NO_REGISTERED_HANDLER;
}

/* Fundamental Interrupt Handlers for all 8 possible pin interrupts */

void PIN_INT0_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(0));
	handlers[0]();
}

void PIN_INT1_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(1));
	handlers[1]();
}

void PIN_INT2_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(2));
	handlers[2]();
}

void PIN_INT3_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(3));
	handlers[3]();
}

void PIN_INT4_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(4));
	handlers[4]();
}

void PIN_INT5_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(5));
	handlers[5]();
}

void PIN_INT6_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(6));
	handlers[6]();
}

void PIN_INT7_IRQHandler(void)
{
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(7));
	handlers[7]();
}




