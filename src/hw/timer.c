/*
 * timer.c
 *
 *  Created on: Oct 6, 2016
 *      Author: Anmol Thind
 */

#include <chip.h>
#include <canpie.h>
#include <toucan.h>
#include <error.h>

tc_time_t timesync_offset = 0;

tcErr tc_timer_init(void)
{
	/* The LPC1549 only has an 8 bit prescaler on the 32 bit timers, so
	*  we have to chain together two SCT's to get a millisecond timer.
	*  SCT0 runs off the main clock at usec rate after the prescaler, a
	*  match register triggers an input to SCT2 every time the counter
	*  reaches 500 usec and this triggers a count on the falling edge of
	*  the SCT2 input, achieving a msec counter
	*/

	LPC_SYSCON->SYSAHBCLKCTRL[0] |= (1 << 11);	// Enables the inmux clock

	// SCT0 SETUP
	LPC_SYSCON->SYSAHBCLKCTRL[1] |= (1 << 2);	// Enables the SCT0 clock

	// Unified timer, auto limit
    LPC_SCT0->CONFIG           |= (1 << 0) | (1 << 17);

    // GCalculate prescale factor so that clock is usec timer
    _U08 prescale = (Chip_Clock_GetSystemClockRate()/1000000);

    // PRE_L[12:5] = 120-1 (SCT clock = 12MHz/120 = 100 KHz)
    LPC_SCT0->CTRL_L           |= ((prescale - 1) << 5);

    LPC_SCT0->MATCH[0].L        = 499;           			// match 0 @ 0.5 msec = 500 usec
    LPC_SCT0->MATCHREL[0].L     = 499;						// Make it 499 because event output triggers on next clock

    LPC_SCT0->EVENT[0].STATE    = 0xFFFF;		// event 0 happens in all state
    LPC_SCT0->EVENT[0].CTRL     = (1 << 12);	// match 0 condition only

    LPC_SCT0->OUT[4].SET        = (1 << 0);		// event 0 will set   SCT0_OUT4
    LPC_SCT0->OUT[4].CLR        = (1 << 0);		// event 0 will clear SCT0_OUT4

    LPC_SCT0->RES               = (3 << 8);		// output 4 toggles on conflict

    LPC_SCT0->CTRL_L           &= ~(1 << 2);	// start timer

    // SCT2 SETUP

    LPC_SYSCON->SYSAHBCLKCTRL[1] |= (1 << 4);       // enable the SCT2 clock

    LPC_INMUX->SCT2_INMUX[0] = 0x1F;				// input 0 reset
    LPC_INMUX->SCT2_INMUX[0] = (uint32_t)0x4;		// input 0 is SCT0_OUT4

    LPC_SCT2->CONFIG           |= (1 << 0) |		// Unified timer
        							(0x2 << 1) |	// Input comes from CLKSEL
    								(0x0 << 3);		// CLKSEL points to input 0

    LPC_SCT2->CTRL_L           |= (0 << 5);			// No prescaler
    LPC_SCT2->CTRL_L           &= ~(1 << 2);        // start timer

    LPC_SYSCON->SYSAHBCLKCTRL[0] &= ~(1 << 11);		// Disable the inmux clock to conserve power

	return tcErr_OK;
}

tc_time_t tc_timer_get(void)
{
	return LPC_SCT2->COUNT_U;
}

tcErr tc_timer_set_timesync(tc_time_t time)
{
	timesync_offset = time - tc_timer_get();
	return tcErr_OK;
}

tc_time_t tc_timer_get_timesync(void) {
	return tc_timer_get() + timesync_offset;
}

tcErr tc_timer_delay_ms(_U32 delay) {
	tc_time_t start_time = tc_timer_get();
	while (tc_timer_get() - start_time < delay) {
		tc_handle();
	}
	return tcErr_OK;
}

tcErr tc_timer_blocking_delay_ms(_U32 delay) {
	tc_time_t start_time = tc_timer_get();
	while (tc_timer_get() - start_time < delay) ;
	return tcErr_OK;
}



