/*
 * uart.c
 *
 *  Created on: Oct 12, 2016
 *      Author: athi
 */

#include <chip.h>
#include <canpie.h>
#include <toucan.h>
#include <error.h>

#include <uart.h>

#include <string.h>
#include <stdarg.h>

/* Private functions for printf functionality */
void UART_putchar(char c);
static void UART_printchar(char **str, int c);
static int UART_prints(char **out, const char *string, int width, int pad);
static int UART_printi(char **out, int i, int b, int sg, int width, int pad, int letbase);
static unsigned UART_printfloat(char **out, double dbl, unsigned frac_len);
static int UART_print(char **out, const char *format, va_list argp);
int UART_sprintf(char *out, const char *format, ...);

/* Default DEBUG UART is UART0 */
TC_UART_T* debug_uart = TC_UART0;

tcErr tc_uart_debug_assign(TC_UART_T* module)
{
	debug_uart = module;
	return tcErr_OK;
}

tcErr tc_uart_tx_pinassign(TC_UART_T* module, _U08 port, _U08 pin)
{
	/* Disables pullups/pulldowns and enable digital mode */
	Chip_IOCON_PinMuxSet(LPC_IOCON, port, pin, (IOCON_MODE_INACT | IOCON_DIGMODE_EN));

	/* UART signal muxing via SWM */
	if (module == LPC_USART0) {
		Chip_SWM_MovablePortPinAssign(SWM_UART0_TXD_O, port, pin);
	}
	else if (module == LPC_USART1) {
		Chip_SWM_MovablePortPinAssign(SWM_UART1_TXD_O, port, pin);
	}
	else if (module == LPC_USART2) {
		Chip_SWM_MovablePortPinAssign(SWM_UART2_TXD_O, port, pin);
	}
	else {
		return tcErr_INVALID_MODULE;
	}

	return tcErr_OK;
}

tcErr tc_uart_rx_pinassign(TC_UART_T* module, _U08 port, _U08 pin)
{
	/* Disables pullups/pulldowns and enable digital mode */
	Chip_IOCON_PinMuxSet(LPC_IOCON, port, pin, (IOCON_MODE_INACT | IOCON_DIGMODE_EN));

	/* UART signal muxing via SWM */
	if (module == LPC_USART0) {
		Chip_SWM_MovablePortPinAssign(SWM_UART0_RXD_I, port, pin);
	}
	else if (module == LPC_USART1) {
		Chip_SWM_MovablePortPinAssign(SWM_UART1_RXD_I, port, pin);
	}
	else if (module == LPC_USART2) {
		Chip_SWM_MovablePortPinAssign(SWM_UART2_RXD_I, port, pin);
	}
	else {
		return tcErr_INVALID_MODULE;
	}

	return tcErr_OK;
}

tcErr tc_uart_init(TC_UART_T* module, _U32 baudrate, _U08 datalen, _U08 parity, _U08 stopbits)
{
	// Note you need to call tc_uart_pinassigns before this function

	/* Use main clock rate as base for UART baud rate divider */
	Chip_Clock_SetUARTBaseClockRate(Chip_Clock_GetMainClockRate(), false);

	Chip_UART_Init(module);
	_U32 cfg = 0;

	switch (datalen) {
	case TC_UART_CFG_DATALEN_7:
		cfg |= UART_CFG_DATALEN_7;
		break;

	case TC_UART_CFG_DATALEN_8:
		cfg |= UART_CFG_DATALEN_8;
		break;

	case TC_UART_CFG_DATALEN_9:
		cfg |= UART_CFG_DATALEN_9;
		break;
	}

	switch (parity) {
	case TC_UART_CFG_PARITY_NONE:
		cfg |= UART_CFG_PARITY_NONE;
		break;

	case TC_UART_CFG_PARITY_EVEN:
		cfg |= UART_CFG_PARITY_EVEN;
		break;

	case TC_UART_CFG_PARITY_ODD:
		cfg |= UART_CFG_PARITY_ODD;
		break;
	}

	switch (stopbits) {
	case TC_UART_CFG_STOPLEN_1:
		cfg |= UART_CFG_STOPLEN_1;
		break;

	case TC_UART_CFG_STOPLEN_2:
		cfg |= UART_CFG_STOPLEN_2;
		break;
	}

	Chip_UART_ConfigData(module, cfg);
	Chip_UART_SetBaud(module, baudrate);
	Chip_UART_Enable(module);
	Chip_UART_TXEnable(module);

	return tcErr_OK;
}

tcErr tc_uart_stop(TC_UART_T* module)
{
	Chip_UART_DeInit(module);
	return tcErr_OK;
}

_S32 tc_uart_send(TC_UART_T* module, _U08 *data, _S32 numBytes)
{
	return Chip_UART_Send(module, data, numBytes);
}

_S32 tc_uart_sendBlocking(TC_UART_T* module, _U08 *data, _S32 numBytes)
{
	return Chip_UART_SendBlocking(module, data, numBytes);
}

_S32 tc_uart_read(TC_UART_T* module, _U08 *data, _S32 numBytes)
{
	return Chip_UART_Read(module, data, numBytes);
}

_S32 tc_uart_readBlocking(TC_UART_T* module, _U08 *data, _S32 numBytes)
{
	return Chip_UART_ReadBlocking(module, data, numBytes);
}

void UART_putchar(char c)
{
	uint8_t c1 = c;
	tc_uart_sendBlocking(debug_uart, &c1, 1);
}

_S32 tc_uart_printf(const char *format, ...) {
	va_list argp;
	int len;
	va_start(argp, format);
	len = UART_print(0, format, argp);
	va_end(argp);
	return len;
}

////////////////////////////////////////////////////////
// Author: David Snowdon (printf implementation HERE) //
////////////////////////////////////////////////////////

static void UART_printchar(char **str, int c) {
	if (str) {
		**str = c;
		++(*str);
	} else {
		(void)UART_putchar(c);
	}
}

#define PAD_RIGHT 1
#define PAD_ZERO 2

static int UART_prints(char **out, const char *string, int width, int pad) {
	register int pc = 0, padchar = ' ';

	if (width > 0) {
		register int len = 0;
		register const char *ptr;
		for (ptr = string; *ptr; ++ptr)
			++len;

		if (len >= width)
			width = 0;
		else
			width -= len;

		if (pad & PAD_ZERO)
			padchar = '0';
	}
	if (!(pad & PAD_RIGHT)) {
		for ( ; width > 0; --width) {
			UART_printchar(out, padchar);
			++pc;
		}
	}
	for ( ; *string ; ++string) {
		UART_printchar(out, *string);
		++pc;
	}
	for ( ; width > 0; --width) {
		UART_printchar(out, padchar);
		++pc;
	}

	return pc;
}

/* the following should be enough for 32 bit int */
#define PRINT_BUF_LEN 12

static int UART_printi(char **out, int i, int b, int sg, int width, int pad, int letbase) {
	char print_buf[PRINT_BUF_LEN];
	register char *s;
	register int t, neg = 0, pc = 0;
	register unsigned int u = i;

	if (i == 0) {
		print_buf[0] = '0';
		print_buf[1] = '\0';
		return UART_prints(out, print_buf, width, pad);
	}

	if (sg && b == 10 && i < 0) {
		neg = 1;
		u = -i;
	}

	s = print_buf + PRINT_BUF_LEN-1;
	*s = '\0';

	while (u) {
		t = u % b;
		if( t >= 10 )
			t += letbase - '0' - 10;
		*--s = t + '0';
		u /= b;
	}

	if (neg) {
		if( width && (pad & PAD_ZERO) ) {
			UART_printchar(out, '-');
			++pc;
			--width;
		}
		else {
			*--s = '-';
		}
	}

	return pc + UART_prints(out, s, width, pad);
}

/* A simple print float implementation. Takes the decimal part, prints it
 * as an int. Subtracts decimal part, and multiplies by 10^digits_past_point
 * and prints that as the fraction part.
 * Warning: There is probably going to be rounding error in here.
 * NOTE: This function should only be used for debugging as it's VERY slow in use
 * it performs many double floating point ops per character printed.
 */
static unsigned UART_printfloat(char **out, double dbl, unsigned frac_len) {
	int len = 0;
	int frac = 0;
	int dec = (int)dbl;
	int i;
	int sign;
	int multiplier=1;

	if (dbl < 0.0)
		sign = -1;
	else
		sign = 1;

	len += tc_uart_printf("%d", dec);

	for (i = 0; i < frac_len; i++){
		if(i==0){
			tc_uart_printf(".");
		}

		frac=((int) (dbl*multiplier*sign))*10; //Stuff already printed to the left mi
		multiplier*=10;
		dec=((int) (dbl * multiplier*sign))-frac;

		tc_uart_printf("%d", dec);

	}

	return len;
}

static int UART_print(char **out, const char *format, va_list argp) {
	int width, pad, dec_len, frac_len, frac;
	int pc = 0;
	char scr[2];

	for (; *format != 0; ++format) {
		if (*format == '%') {
			++format;
			width = pad = dec_len = frac_len = frac = 0;
			if (*format == '\0') break;
			if (*format == '%') goto out;
			if (*format == '-') {
				++format;
				pad = PAD_RIGHT;
			}
			while (*format == '0') {
				++format;
				pad |= PAD_ZERO;
			}
			for ( ; (*format >= '0' && *format <= '9') || *format == '.'; ++format) {
				width *= 10;
				width += *format - '0';

				if (*format == '.') {
					dec_len = width;
					width = 0;
					frac = 1;
				}

				if (frac == 1)
					frac_len = width;

			}
			if( *format == 's' ) {
				char *s = va_arg(argp, char *);
				pc += UART_prints (out, s?s:"(null)", width, pad);
				continue;
			}
			if( *format == 'd' ) {
				int i = va_arg(argp, int);
				pc += UART_printi (out, i, 10, 1, width, pad, 'a');
				continue;
			}
			if( *format == 'f' ) {
				double f = va_arg(argp, double);
				pc += UART_printfloat (out, f, frac_len);
				continue;
			}
			if( *format == 'x' ) {
				int i = va_arg(argp, int);
				pc += UART_printi (out, i, 16, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'X' ) {
				int i = va_arg(argp, int);
				pc += UART_printi (out, i, 16, 0, width, pad, 'A');
				continue;
			}
			if( *format == 'u' ) {
				unsigned int i = va_arg(argp, unsigned int);
				pc += UART_printi (out, (int)i, 10, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'c' ) {
				/* char are converted to int then pushed on the stack */
				scr[0] = (char)va_arg(argp, int);
				scr[1] = '\0';
				pc += UART_prints (out, scr, width, pad);
				continue;
			}
		}
		else {
		out:
			UART_printchar (out, *format);
			++pc;
		}
	}
	if (out) **out = '\0';
	return pc;
}

int UART_sprintf(char *out, const char *format, ...) {
	va_list argp;
	int len;
	va_start(argp, format);
	len = UART_print(&out, format, argp);
	va_end(argp);
	return len;
}


