/*
 * toucan.c
 *
 *  Created on: Oct 16, 2016
 *      Author: athi
 */

//#include <canpie.h>
#include <toucan.h>

#include <timer.h>
#include <error.h>
#include <uart.h>
#include <gpio.h>
#include <message.h>
#include <can.h>

//#define TC_DEBUG

#define HEARTBEAT_PERIOD	1000

tc_time_t last_heartbeat;

_U16 tc_node_id = 'a';

tcErr tc_init(_U16 node_id)
{
	tc_node_id = node_id;

	// TODO: Enable WDT

	/* Start GPIO */
	tc_gpio_init();

	/* Start Timer */
	tc_timer_init();

	/* Start the USB tied UART module */
    tc_uart_tx_pinassign(TC_UART0, UART_USB_TX_PORT, UART_USB_TX_PIN);
    tc_uart_rx_pinassign(TC_UART0, UART_USB_RX_PORT, UART_USB_RX_PIN);
    tc_uart_init(TC_UART0, 115200, TC_UART_CFG_DATALEN_8, TC_UART_CFG_PARITY_NONE, TC_UART_CFG_STOPLEN_1);

    /* Start the CAN module */
    tc_can_init(CP_BAUD_500K);

	return tcErr_OK;
}

tcErr tc_set_mode(tcMode mode)
{
	return tcErr_NOT_IMPLEMENTED;
}

tcErr tc_handle(void)
{
	/* Handle heartbeat */
	tc_time_t curr_time = tc_timer_get();
	if (curr_time - last_heartbeat > HEARTBEAT_PERIOD) {
		toucan_send_heartbeat();
		//tc_uart_printf("HEARTBEAT SENT \n");
		last_heartbeat = tc_timer_get();
	}

	/* Handle periodic broadcasts */
	toucan_handle_periodics();

	/* Transmit some queued messages */
	toucan_send_queued_messages();

	/* TODO: Feed Watchdog */

	return tcErr_OK;
}

_U16 toucan_get_node_id(void)
{
	return tc_node_id;
}
